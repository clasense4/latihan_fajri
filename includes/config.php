<?php
//error_reporting (E_NONE);
//error_reporting (E_ALL);

define ('APPNAME', 'latihan');
define ('SERVERNAME', $_SERVER["SERVER_NAME"]);
define ('DBNAME', 'latihan');
define ('MODULE_DIR', 'modules');
define ('SYSTEM_DIR', 'system');

define ('APPTITLE', 'Sistem Pengendalian Intern (SPIN) Keuangan Dikdas');
define ('SESSNAME', APPNAME.'_session');

define ('NAMA_TAHUN', '2012');
define ('NAMA_UNIT_SINGKAT', 'DIKDAS');
